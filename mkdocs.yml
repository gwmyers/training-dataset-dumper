site_name: FTAG Dumpster documentation
site_description: Documentation for the ATLAS FTAG dataset dumper
site_author: training-dataset-dumper team
site_url: https://training-dataset-dumper.docs.cern.ch/
copyright: Copyright &copy; 2002 - 2023 CERN for the benefit of the ATLAS collaboration

repo_name: GitLab
repo_url: https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/
edit_uri: tree/main/docs

theme:
  name: material
  logo: assets/dumpster.png
  favicon: assets/dumpster.png
  features:
    - navigation.instant
    - navigation.tracking
    - navigation.sections
    - navigation.indexes
    - navigation.top
    - content.code.copy
    - content.action.edit

nav:
  - index.md

  - Basic Usage:
    - Installation: installation.md
    - Local Dumps: basic_usage.md
    - Grid Dumps: grid.md
    - Configuration: configuration.md

  - Advanced Usage:
    - Installation: advanced.md
    - Example Use Cases: advanced_usage.md

  - Development:
      - Contributing: contributing.md
      - Adding CI Tests: tests.md

  - Outputs:
      - Tools & Defaults: outputs.md
      - PFlow Jet Outputs: vars_pflow.md
      - Track Jet Outputs: vars_trackjets.md

plugins:
  - search
  - mermaid2
  - markdownextradata
  - git-revision-date-localized:
      enable_creation_date: true
      type: date

markdown_extensions:
  - admonition
  - codehilite
  - pymdownx.arithmatex
  - pymdownx.details
  - pymdownx.inlinehilite
  - pymdownx.smartsymbols
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - toc:
      permalink: "#"
  - footnotes

extra_javascript:
    - 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.0/MathJax.js?config=TeX-MML-AM_CHTML'
