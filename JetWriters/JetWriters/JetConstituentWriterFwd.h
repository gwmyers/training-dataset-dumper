#ifndef JET_CONSTITUENT_WRITER_FWD_H
#define JET_CONSTITUENT_WRITER_FWD_H

#include <tuple>

template <typename T, typename A = std::tuple<>> class JetConstituentWriter;

#endif
