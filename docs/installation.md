This section describes how to install the basic AnalysisBase dataset dumper. This is the recommend workflow in most cases.
The training dataset dumper can be installed either locally or be run by using a Docker image.
Both options are outlined below.

### Local installation

First, retrieve the project by cloning the git repository.
If you plan to make any changes to the repository, you should instead fork the repository and clone your fork. You can find out more in the [contributing guidelines](contributing.md).

```bash
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
```

Then, install the project locally by setting up a release and compiling the code.

```bash
source training-dataset-dumper/setup/analysisbase.sh
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
```

??? info "What to do if the setup script crashes"

	If a setup script (e.g. `setup/analysisbase.sh`) crashes, you should first report this by [opening an issue](contributing.md#adding-features-fixing-bugs) on GitLab.
	The script may have crashed because you used an unsupported shell.
	The main supported shell is [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)), though there is also
	experimental support for [`zsh`](https://en.wikipedia.org/wiki/Z_shell).
	If you are using a different shell and experience problems, either open a bash shell for working with the dumper, or set up an analysis release by hand.
	
	To do the latter, first find the recommended analysis release version in the [`.gitlab-ci.yml`]({{repo_url}}-/blob/main/.gitlab-ci.yml) file,
	in the line with `BUILD_IMAGE: $DOCKER_CACHE/atlas/analysisbase:{analysis_release_version}`.
	You can then manually set up a release by entering
	```bash
	# use the analysis release version which is documented in
	# the .gitlab-ci.yml file and not the dummy example below
	ANALYSIS_RELEASE=22.2.95
	setupATLAS
	asetup AnalysisBase,${ANALYSIS_RELEASE}
	```

As the final step, the following file needs to be sourced in order to add the executables to the system path
```bash
source build/x*/setup.sh
```

For convenience, all the commands (aside from setting up a release) are packaged into a `build.sh` script, which you can run with
```bash
source ./training-dataset-dumper/setup/build.sh
```

This script will set up a fresh `build/` directory in the directory above `training-dataset-dumper/`.
Similarly, `rebuild.sh` can be used to rebuild the code without setting up a fresh directory
(assuming you have already built the code at some point in the past).


#### Running a test

The package includes a [test script]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/test/test-dumper), which will download and process a small test sample. To run a test, use

```bash
test-dumper pflow
```

This script will run the program in [`BTagTrainingPreprocessing/utils/dump-single-btag.cxx`]({{repo_url}}-/blob/main/BTagTrainingPreprocessing/util/dump-single-btag.cxx) which will dump some xAOD information to HDF5.
The script takes a mandatory argument which specifies which configuration file to use for the test job.
By default the output from test-dumper will be stored in a random directory under `/tmp`, but this can be configured, see

```bash
test-dumper -h
```

for options. You can inspect the contents of this file with

```bash
h5ls path/to/output.h5
```

again see `-h` for more options. Also see [the `h5ls` tab-complete script for bash users][h5tab].

[h5tab]: https://github.com/dguest/_h5ls

??? info "Issues when building or running the code"

	The first thing to try is completely removing your `build/` directory and setting everything up from scratch in a fresh shell.


#### Restoring the setup

The next time you want to use the utility run from the project directory

```bash
source training-dataset-dumper/setup/analysisbase.sh
source build/x*/setup.sh
```

### Docker containers

You can run the training dataset dumper in a [Docker container](https://www.docker.com/resources/what-container). This is a convenient way to run the code if you don't have access to `/cvmfs/`.

Complete images are created automatically from the `main` branch and updated for every modification using Continuous Integration. Note, that you need to specify the tag `main` to run the training-dataset-dumper for release 22:
```
gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
```

#### Developing in containers
The docker image contains a static version of the dataset dumper code. If you want to actively develop the code, the recommended way to do so is to check out a local version of the project (but not install it), then start up a docker container which has the local directory mounted.
This provides you with an ATLAS release which contains the dependencies the code needs to build and run.

Example:
```bash
# get code
git clone ssh://git@gitlab.cern.ch:7999/atlas-flavor-tagging-tools/training-dataset-dumper.git
# start docker container and mount current directory inside container
docker run --rm -it -v $PWD:/home/workdir --workdir /home/workdir gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
# compile code: no need to source a setup script with "asetup" inside of a docker container
mkdir build
cd build
cmake ../training-dataset-dumper
make
cd ..
# add executables to system path
source build/x*/setup.sh
```

You aren't required to build the dataset dumper in the above image: any relatively recent [`AthAnalysis`](https://hub.docker.com/r/atlas/athanalysis/tags) or [`AnalysisBase`](https://hub.docker.com/r/atlas/analysisbase/tags) image in release 22 will accomplish the same thing.

#### Launching containers using Docker (local machine)
If you work on a local machine with Docker installed, you can run Umami with this command:
```bash
docker run --rm -it gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
```

You can mount local directories with the `-v` argument:
```bash
docker run --rm -it -v $PWD:/home/workdir --workdir /home/workdir gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
```


#### Launching containers using Singularity (lxplus/institute cluster)
If you work on a node of your institute's computing centre or on CERN's `lxplus`, you don't have access to Docker.
Instead, you can use [singularity](https://sylabs.io/guides/3.7/user-guide/introduction.html), which provides similar features.

You can run the training dataset dumper in singularity with the following command:
```bash
singularity --silent run docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
```

You can mount local directories with the `-B` argument:
```bash
singularity --silent run -B /cvmfs:/cvmfs -B /afs:/afs -B $PWD:/home/workdir docker://gitlab-registry.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper:main
```
