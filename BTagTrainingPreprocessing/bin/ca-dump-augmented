#!/usr/bin/env python

"""Augmented dumpster script

This script should provide some other augmentation algorithms that we
should eventually be running in derivations.

"""

import sys
import json
from BTagTrainingPreprocessing import dumper
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

def get_folder():
    ca = ComponentAccumulator()
    common = dict(
        eventID='EventInfo.mcEventNumber',
        salt=42,
        jetCollection='AntiKt4EMPFlowJets',
        associations=['constituentLinks', 'GhostTrack'],
        jetVariableSaltSeeds={
            'constituentLinks': 0,
            'GhostTrack': 1,
        },
    )
    ca.addEventAlgo(
        CompFactory.FoldDecoratorAlg(
            'withHits',
            **common,
            constituentChars=json.dumps({
                'GhostTrack': [
                    'numberOfPixelHits',
                    'numberOfSCTHits',
                    # 'numberOfTRTHits'
                ]
            }),
            constituentSaltSeeds={
                'numberOfPixelHits': 2,
                'numberOfSCTHits': 3,
                # 'numberOfTRTHits': 4,
            },
            jetFoldHash='jetFoldHash',
        )
    )

    ca.addEventAlgo(
        CompFactory.FoldDecoratorAlg(
            'noHits',
            **common,
            jetFoldHash='jetFoldHash_noHits',
        )
    )

    return ca

def run():

    parser = dumper.base_parser(__doc__)
    args = parser.parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)
    ca.merge(get_folder())
    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
