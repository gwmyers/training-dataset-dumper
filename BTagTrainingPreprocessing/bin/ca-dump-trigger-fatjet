#!/usr/bin/env python

"""
Dumper for large-R jets that are reconstructed in trigger
"""

import sys
from BTagTrainingPreprocessing import dumper, mctc
from BTagTrainingPreprocessing.trigger import getLabelingBuilderAlg
from JetTagCalibration.JetTagCalibConfig import JetTagCalibCfg
from BTagging.BTagConfig import BTagAlgsCfg
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg
from ParticleJetTools.ParticleJetToolsConfig import (
    getJetDeltaRFlavorLabelTool)

def getRequisiteAlgsCA(cfgFlags):
    fs_tracks = 'HLT_IDTrack_FS_FTF'
    fs_vertices = 'HLT_IDVertex_FS'
    ca = ComponentAccumulator()
    ca.merge(mctc.getMCTC())
    ca.addEventAlgo(CompFactory.ParentLinkDecoratorAlg(
            name = "ParentLinkDecoratorAlg"))
    labelAlg = getLabelingBuilderAlg(cfgFlags)
    ca.addEventAlgo(labelAlg)
    DRTool = getJetDeltaRFlavorLabelTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
        "HLT_TrackJetDeltaRLabelingAlg",
        Decorators = [DRTool],
        JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"
    ))
    ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
        name = "PoorMansIpAugmenter_HLT_IDTrack_FS_FTF",
        prefix = 'btagIp_',
        trackContainer = fs_tracks,
        primaryVertexContainer = fs_vertices))

    OverlapDecoTool = CompFactory.TriggerVRJetOverlapDecoratorTool()
    ca.addEventAlgo(CompFactory.JetDecorationAlg(
            "HLT_VRJetOverlapDecoratorAlg",
            Decorators = [OverlapDecoTool],
            JetContainer = "HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets"))
    ca.merge(JetTagCalibCfg(cfgFlags))
    ca.merge(BTagAlgsCfg(
        cfgFlags,
        JetCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        nnList=['BTagging/20211216trig/dips/AntiKt4EMPFlow/network.json'],
        trackCollection= fs_tracks,
        primaryVertices= fs_vertices,
        muons='',
        renameTrackJets = False,
        AddedJetSuffix=''
    ))
    ca.merge(JetParticleAssociationAlgCfg(
        cfgFlags,
        JetCollection="HLT_AntiKt10EMPFlowCSSKSoftDropBeta100Zcut10Jets_nojcalib_ftf",
        InputParticleCollection="HLT_AntiKtVR30Rmax4Rmin02PV0TrackJets",
        OutputParticleDecoration="PV0TrackJets"))

    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()
    top_level_ca = dumper.getMainConfig(cfgFlags, args)
    top_level_ca.merge(getRequisiteAlgsCA(cfgFlags))
    top_level_ca.merge(dumper.getDumperConfig(args))
    return top_level_ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
