#include "HitWriter.hh"
#include "HitDecorator.hh"
#include "HitWriterConfig.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

#include "xAODTracking/Vertex.h"
#include "xAODJet/Jet.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"
#include "xAODEventInfo/EventInfo.h"

/////////////////////////////////////////////
// Internal classes
/////////////////////////////////////////////

namespace {
// internal structures
  Amg::Vector3D p3(const xAOD::IParticle& p) {
    auto p4 = p.p4();
    return {p4.X(), p4.Y(), p4.Z()};
  }
  Amg::Vector3D p3(const xAOD::IParticle* p) {
    return p3(*p);
  }
  Amg::Vector3D p3(const xAOD::TrackMeasurementValidation* p) {
    return {p->globalX(), p->globalY(), p->globalZ()};
  }
  Amg::Vector3D p3(const xAOD::Vertex& p) {
    return {p.x(), p.y(), p.z()};
  }

  struct HitOutputs {
    Amg::Vector3D global;
    Amg::Vector3D origin;
    Amg::Vector3D local;
    const xAOD::TrackMeasurementValidation* hit;
    const xAOD::Jet* jet;
  private:
    Eigen::Vector3d m_jab;
  public:
    HitOutputs(const xAOD::TrackMeasurementValidation* h, const xAOD::Jet* j, const Amg::Vector3D& the_origin):
      global(p3(h)),
      origin(the_origin),
      local(global - the_origin),
      hit(h),
      jet(j)
      {
        // I want to compute jab coordinates: jet projection, adjacent
        // projection, beamline projection. The "adjacent" projection
        // is defined to be orthogonal to the jet and beam, but this
        // isn't a fully orthogonal basis. I won't expose the vector
        // as public because we've patched in a lot of methods that
        // assume orthogonality for Amg vectors.

        // use zhat as beamline for now
        Eigen::Vector3d bhat(0,0,1);
        Eigen::Vector3d jet = p3(j);
        Eigen::Vector3d jhat = jet.normalized();
        Eigen::Vector3d ahat = bhat.cross(jhat);

        // build the matrix m that maps the jab displacement such that
        // m*job = detector
        Eigen::Matrix3d m;
        m << jhat, ahat, bhat;
        // now solve this for jab = m^-1 * detector
        m_jab = m.inverse() * local;
      }
    double j() const { return m_jab(0); }
    double a() const { return m_jab(1); }
    double b() const { return m_jab(2); }
  };

  template <typename I, typename F>
  H5Utils::Compression compression_level(F) {
    using T = decltype(std::declval<F>()(std::declval<I>()));
    if (std::is_floating_point<T>::value) {
      return H5Utils::Compression::HALF_PRECISION;
    }
    return H5Utils::Compression::STANDARD;
  }

}


typedef std::function<float(const HitOutputs&)> FloatFiller;
class HitConsumers: public H5Utils::Consumers<const HitOutputs&> {};
class HitOutputWriter: public H5Utils::Writer<1,const HitOutputs&>
{
public:
  HitOutputWriter(H5::Group& file,
                  const std::string& name,
                  const HitConsumers& cons,
                  size_t size):
    H5Utils::Writer<1,const HitOutputs&>(file, name, cons, {{size}}) {}
};

Acc<char> isSct("isSCT");

///////////////////////////////////
// Class definition starts here
///////////////////////////////////
HitWriter::HitWriter(
  H5::Group& output_file,
  const HitWriterConfig& config):
  m_hdf5_hit_writer(nullptr),
  m_dist_to_jet(config.dist_to_jet),
  m_save_endcap_hits(config.save_endcap_hits),
  m_save_only_clean_hits(config.save_only_clean_hits),
  m_bec(Acc<int>("bec"))
{

  HitConsumers fillers;
  add_hit_variables(fillers, config);

  // build the output dataset
  if (config.name.size() == 0) {
    throw std::logic_error("hit output name not specified");
  }
  if (config.output_size == 0) {
    throw std::logic_error("can't make an output writer with no hits");
  }

  m_hdf5_hit_writer.reset(
    new HitOutputWriter(
      output_file, config.name, fillers, config.output_size));
}

HitWriter::~HitWriter() {
  if (m_hdf5_hit_writer) m_hdf5_hit_writer->flush();
}

HitWriter::HitWriter(HitWriter&&) = default;


HitWriter::TMVV HitWriter::sortHitsByDR(
  const std::vector<const xAOD::TrackMeasurementValidation*>& in,
  const xAOD::Jet& jet,
  const xAOD::Vertex& vx) {
  std::vector<std::pair<float, TMVV::value_type>> out;
  for (const auto* hit: in) {
    // Calculate dR of hit to jet
    Amg::Vector3D local = p3(hit) - p3(vx);
    float dist = p3(jet).deltaR(local);

    if (dist < m_dist_to_jet) {
      out.emplace_back(dist, hit);
    }
  }
  std::sort(out.begin(), out.end(),
            [](const auto& p1, const auto& p2) { return p1 < p2; });
  TMVV out_sec;
  for (const auto& o: out) out_sec.push_back(o.second);
  return out_sec;
}


HitWriter::TMVV HitWriter::sortHitsByDPhi(
  const std::vector<const xAOD::TrackMeasurementValidation*>& in,
  const xAOD::Jet& jet,
  const Amg::Vector3D& beamspot) {
  std::vector<std::pair<float, TMVV::value_type>> out;
  for (const auto* hit: in) {
    // Calculate dR of hit to jet
    Amg::Vector3D local = p3(hit) - beamspot;
    float dist = std::abs(p3(jet).deltaPhi(local));
    if (dist < m_dist_to_jet) {
      out.emplace_back(dist, hit);
    }
  }
  std::sort(out.begin(), out.end(),
            [](const auto& p1, const auto& p2) { return p1 < p2; });
  TMVV out_sec;
  for (const auto& o: out) out_sec.push_back(o.second);
  return out_sec;
}


void HitWriter::write(
  const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
  const xAOD::Jet& jet,
  const xAOD::Vertex& vx)
{
  if (!m_hdf5_hit_writer) return;

  // sort hits in decreasing dR from jet axis
  auto sortedHits = sortHitsByDR(hits, jet, vx);

  write_generic(sortedHits, jet, p3(vx));
}

void HitWriter::write(
  const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
  const xAOD::Jet& jet,
  const xAOD::EventInfo& ei)
{
  if (!m_hdf5_hit_writer) return;

  Amg::Vector3D beamspot(ei.beamPosX(), ei.beamPosY(), ei.beamPosZ());
  auto sortedHits = sortHitsByDPhi(hits, jet, beamspot);

  write_generic(sortedHits, jet, beamspot);
}


void HitWriter::write_generic(
  const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
  const xAOD::Jet& jet,
  const Amg::Vector3D& vx)
{
  std::vector<HitOutputs> hit_outputs;
  for (const auto* hit: hits) {

    // Check if the hit should be saved (e.g. EC vs barrel)
    bool isBarrel = (m_bec(*hit) == 0);
    bool saveHit = isBarrel || (!isBarrel && m_save_endcap_hits);

    // Only save hits that we want to save
    if (saveHit) {
      hit_outputs.emplace_back(hit, &jet, vx);
    }
  }
  m_hdf5_hit_writer->fill(hit_outputs);
}

void HitWriter::write_dummy() {
  if (m_hdf5_hit_writer) {
    std::vector<HitOutputs> hit_outputs;
    m_hdf5_hit_writer->fill(hit_outputs);
  }
}

void HitWriter::add_hit_variables(
  HitConsumers& vars,
  const HitWriterConfig& cfg)
{
  using VarAdder = std::function<void(HitConsumers&)>;
  using HO = HitOutputs;
  // convenience function to avoid using the same key twice (once to
  // name the output, once to index the function)
  auto mkadder = [](const std::string n, auto f, auto d) {
    return std::make_pair(n, [n,f,d](HitConsumers& c) {
      c.add(n, f, d, compression_level<HO>(f));
    });
  };
  std::map<std::string, VarAdder> var_adders {
    // valid flag, true for any hit that is defined
    mkadder("valid", [](const HO&) { return true; }, false),
    // Cartesian coordinates
    mkadder("x_local", [](const HO& h) -> float { return h.local.x(); }, NAN),
    mkadder("y_local", [](const HO& h) -> float { return h.local.y(); }, NAN),
    mkadder("z_local", [](const HO& h) -> float { return h.local.z(); }, NAN),
    mkadder("x_global", [](const HO& h) -> float { return h.global.x(); }, NAN),
    mkadder("y_global", [](const HO& h) -> float { return h.global.y(); }, NAN),
    mkadder("z_global", [](const HO& h) -> float { return h.global.z(); }, NAN),
    // beamline projection coordinates
    mkadder("j", [](const HO& h) -> float { return h.j(); }, NAN),
    mkadder("a", [](const HO& h) -> float { return h.a(); }, NAN),
    mkadder("b", [](const HO& h) -> float { return h.b(); }, NAN),
    // angular coordinates
    mkadder("deta", [](const HO& h) -> float {
      return h.local.eta() - p3(h.jet).eta();
    }, NAN),
    mkadder("dphi", [](const HO& h) -> float {
        return h.local.deltaPhi(p3(h.jet));
    }, NAN),
    mkadder("abs_deta", [](const HO& h) -> float {
      return std::copysign(1.0, p3(h.jet).eta()) * (
        h.local.eta() - p3(h.jet).eta());
    }, NAN),
    mkadder("radius",[](const HO& h) -> float { return h.global.perp(); }, NAN),
    mkadder("radius_local",[](const HO& h) -> float {
      return h.local.perp(); }, NAN),

    // Detector info
    //
    // Hit layer (integer 0-4) and isBarrel (bec=0: barrel, bec=+-2: endcap)
    mkadder("layer", [acc=Acc<int>("layer")](const HO& h) -> char {
      return acc(*h.hit);
    }, char(-1)),
    mkadder("isBarrel", [acc=Acc<int>("bec")](const HO& h){
      return (acc(*h.hit) == 0);
    }, false),
    //this variable is saved as 1 if the hit come from the SCT, 0 if
    //the hit come from the pixel, -1 elsewhere.
    mkadder("isSCT", [](const HO& h){ return isSct(*h.hit); }, char(-1)),

    //The following variables are settet as -1 when the hit comes from
    // the SCT subdetector, because that hits don't have these
    // information into the AOD.  Information about hit quality
    mkadder("isFake", [acc=Acc<char>("isFake")](const HO& h){
      return isSct(*h.hit) ? char(-1) :  acc(*h.hit);
    }, char(-1)),
    mkadder("broken", [acc=Acc<char>("broken")](const HO& h){
      return isSct(*h.hit) ? char(-1) :  acc(*h.hit);
    }, char(-1)),
    mkadder("DCSState", [acc=Acc<int>("DCSState")](const HO& h){
      return isSct(*h.hit) ? -1 : acc(*h.hit);
    }, -1),
    mkadder("hasBSError", [acc=Acc<int>("hasBSError")](const HO& h){
      return isSct(*h.hit) ? -1 :  acc(*h.hit);
    }, -1),

    // Information about split hits
    mkadder("isSplit", [acc=Acc<int>("isSplit")](const HO& h) -> bool {
      return isSct(*h.hit) ? false :  acc(*h.hit);
    }, false),
    mkadder("splitProbability1", [acc=Acc<float>("splitProbability1")](const HO& h){
      return isSct(*h.hit) ? NAN :  acc(*h.hit);
    }, NAN),
    mkadder("splitProbability2", [acc=Acc<float>("splitProbability2")](const HO& h){
      return isSct(*h.hit) ? NAN :  acc(*h.hit);
    }, NAN),
  };

  for (const std::string& var: cfg.variables) {
    if (!var_adders.count(var)) {
      throw std::runtime_error("hit variable '" + var + "' isn't defined");
    }
    var_adders.at(var)(vars);
  }


}
