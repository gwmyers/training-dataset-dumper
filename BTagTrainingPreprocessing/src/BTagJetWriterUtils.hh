#ifndef BTAG_JET_WRITER_UTILS_H
#define BTAG_JET_WRITER_UTILS_H

#include "HDF5Utils/Writer.h"

#include <string>
#include <vector>
#include <map>

struct BTagVariableMaps;
struct BTagJetWriterBaseConfig;

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

// structure to serve as base for lambda functions
struct JetOutputs {
  JetOutputs();
  const xAOD::Jet* jet;
  const xAOD::Jet* parent;
  const xAOD::EventInfo* event_info;
};

using JetConsumers = H5Utils::Consumers<const JetOutputs&>;
using FloatFiller = std::function<float(const JetOutputs&)>;

void add_jet_variables(JetConsumers&, const BTagJetWriterBaseConfig&,
                       H5Utils::Compression half_prec);

void add_jet_int_variables(JetConsumers&,
                           const std::vector<std::string>&);
void add_parent_jet_int_variables(JetConsumers&,
                                  const std::vector<std::string>&);
void add_parent_fillers(JetConsumers&);
void add_event_info(JetConsumers&,
                    const std::vector<std::string>&,
                    H5Utils::Compression);

class IJetOutputWriter
{
public:
  virtual ~IJetOutputWriter() = default;
  virtual void fill(const std::vector<JetOutputs>& jo) = 0;
  virtual void flush() = 0;
};

class JetOutputWriter: public IJetOutputWriter
{
private:
  H5Utils::Writer<0,const JetOutputs&> m_writer;
public:
  JetOutputWriter(H5::Group& file,
                  const std::string& name,
                  const JetConsumers& cons);
  void fill(const std::vector<JetOutputs>& jos) override;
  void flush() override;
};

class JetOutputWriter1D: public IJetOutputWriter
{
private:
  H5Utils::Writer<1,const JetOutputs&> m_writer;
public:
  JetOutputWriter1D(H5::Group& file,
                    const std::string& name,
                    const JetConsumers& cons,
                    size_t n_jets);
  void fill(const std::vector<JetOutputs>& jos) override;
  void flush() override;
};



#endif
