# set up some grid programs

# setup ATLAS
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo "=== running setupATLAS ==="
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q

# define functions
trap "_cleanup; trap - INT" INT # Note! using INT required for zsh
_cleanup() {
    unset -f _voms_proxy_long
    unset -f _cleanup
    echo "cleaning up"
}

_voms_proxy_long ()
{
    if ! type voms-proxy-info &> /dev/null; then
        echo "voms not set up!" 1>&2
        return 1
    fi
    local VOMS_ARGS="--voms atlas";
    if voms-proxy-info --exists --valid 24:00; then
        local TIME_LEFT=$(voms-proxy-info --timeleft);
        local HOURS=$(( $TIME_LEFT / 3600 ));
        local MINUTES=$(( $TIME_LEFT / 60 % 60 - 1 ));
        local NEW_TIME=$HOURS:$MINUTES;
        VOMS_ARGS+=" --noregen --valid $NEW_TIME";
    else
        VOMS_ARGS+=" --valid 96:00";
    fi;
    eval "voms-proxy-init $VOMS_ARGS" # Note! using eval required for zsh
}

if ! _voms_proxy_long; then
    if [[ ${1-x} == dry-run ]]; then
        echo "ignoring missing voms" 1>&2
        if [[ -z ${AtlasBuildStamp+set} ]]; then
            export AtlasBuildStamp=DUMMY_BUILD_STAMP
            echo "setting AtlasBuildStamp=${AtlasBuildStamp}" 1>&2
        fi
    else
        return 1
    fi
fi
if ! lsetup panda -q; then return 1; fi
if ! lsetup git -q; then return 1; fi
