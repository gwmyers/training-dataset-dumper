
"""
Dumper interface

"""

import argparse
import os
from pathlib import Path
import json

from GaudiKernel.Configurable import DEBUG

from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg

from AnalysisAlgorithmsConfig.ConfigText import combineConfigFiles

class DumperHelpFormatter(
    argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
):
    ...


def base_parser(description, add_help=True):
    parser = argparse.ArgumentParser(description=description, formatter_class=DumperHelpFormatter, add_help=add_help)
    parser.add_argument(
        "input_files", nargs="+",
        help="comma or space separated list of input filenames")
    parser.add_argument(
        "-o", "--output", default=Path("output.h5"), type=Path,
        help="output filename")
    parser.add_argument(
        "-c", "--config-file", required=True, type=Path,
        help="job configuration file")
    parser.add_argument(
        "-m", "--max-events",
        type=int, nargs="?",
        help="number of events to process")
    parser.add_argument(
        "-i", "--event-print-interval",
        type=int, default=500,
        help="set output frequency")
    parser.add_argument(
        '-p', '--force-full-precision',
        action='store_true',
        help='force all half-precision outputs to full precision')
    parser.add_argument(
        '--FPEAuditor',
        action='store_true',
        help='error if floating point exception occurs (requires Athena)')
    parser.add_argument(
        '--run-truth-labelling',
        type=str,
        required=False,
        help='re-run ftag* truth labelling, new labels will be given a suffix using this string'
    )
    parser.add_argument(
        "-d", "--debug",
        action="store_true",
        help="set output level to DEBUG")
    parser.add_argument(
        "-g", "--debugger",
        action="store_true",
        help="attach debugger at execute step")
    return parser


def update_cfgFlags(cfgFlags, args):

    # parse input files
    if len(args.input_files) > 1:
        if any("," in f for f in args.input_files):
            raise ValueError(
                "you provided a mix of spaces and commas"
                " in your input file list, this is probably an error"
            )
        cfgFlags.Input.Files = [str(x) for x in args.input_files]
    else:
        cfgFlags.Input.Files = str(args.input_files[0]).rstrip(",").split(",")

    # set number of events to process
    if args.max_events:
        cfgFlags.Exec.MaxEvents = args.max_events

    # set output level
    if args.debug:
        cfgFlags.Exec.OutputLevel = DEBUG

    # attach debugger
    if args.debugger:
        prepGdb()
        cfgFlags.Exec.DebugStage = "init"

    # error on floating point exceptions
    if args.FPEAuditor:
        cfgFlags.Exec.FPE = -1


def prepGdb():
    """
    Running gdb is sometimes more difficult than it should be, this
    runs some workarounds for the debugger in images.
    """
    # The environment variable I'm checking here just happens to be
    # set to a directory you would not have access to outside images.
    if os.environ.get('LCG_RELEASE_BASE') == '/opt/lcg':
        del os.environ['PYTHONHOME']


def getMainConfig(flags, args):
    ca = MainServicesCfg(flags)
    ca.merge(PoolReadCfg(flags))
    ca.addService(CompFactory.AthenaEventLoopMgr(
        EventPrintoutInterval=args.event_print_interval))
    # Avoid stack traces to the exception handler. These traces
    # aren't very useful since they just point to the handler, not
    # the original bug.
    ca.addService(CompFactory.ExceptionSvc(Catch="NONE"))
    return ca


def combinedConfig(config_file):
    with open(config_file, 'r') as cfg:
        config_dict = json.load(cfg)
    combineConfigFiles(
        config_dict,
        config_file.parent,
        fragment_key='file')
    return config_dict


def getDumperConfig(args, config_dict=None):
    ca = ComponentAccumulator()

    output = CompFactory.H5FileSvc(path=str(args.output))
    ca.addService(output)

    if config_dict is None:
        config_dict = combinedConfig(args.config_file)
    if args.run_truth_labelling:
        run_truth_labelling(ca, args.run_truth_labelling)

    btagAlg = CompFactory.SingleBTagAlg(f'{args.config_file.stem}DatasetDumper')
    btagAlg.output = output
    btagAlg.configJson = json.dumps(config_dict)
    btagAlg.forceFullPrecision = args.force_full_precision
    ca.addEventAlgo(btagAlg)

    return ca

def run_truth_labelling(ca, suffix):
    sub = ComponentAccumulator()
    trackTruthOriginTool = CompFactory.InDet.InDetTrackTruthOriginTool(isFullPileUpTruth=False)

    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
        'TruthParticleDecoratorAlg',
        trackTruthOriginTool=trackTruthOriginTool,
        ftagTruthOriginLabel='ftagTruthOriginLabel'+suffix,
        ftagTruthVertexIndex='ftagTruthVertexIndex'+suffix,
        ftagTruthTypeLabel='ftagTruthTypeLabel'+suffix,
        ftagTruthSourceLabel='ftagTruthSourceLabel'+suffix,
        ftagTruthParentBarcode='ftagTruthParentBarcode'+suffix,
    ))
    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
        'TrackTruthDecoratorAlg',
        trackTruthOriginTool=trackTruthOriginTool,
        acc_ftagTruthVertexIndex='ftagTruthVertexIndex'+suffix,
        acc_ftagTruthTypeLabel='ftagTruthTypeLabel'+suffix,
        acc_ftagTruthSourceLabel='ftagTruthSourceLabel'+suffix,
        acc_ftagTruthParentBarcode='ftagTruthParentBarcode'+suffix,
        dec_ftagTruthOriginLabel='ftagTruthOriginLabel'+suffix,
        dec_ftagTruthVertexIndex='ftagTruthVertexIndex'+suffix,
        dec_ftagTruthTypeLabel='ftagTruthTypeLabel'+suffix,
        dec_ftagTruthSourceLabel='ftagTruthSourceLabel'+suffix,
        dec_ftagTruthBarcode='ftagTruthBarcode'+suffix,
        dec_ftagTruthParentBarcode='ftagTruthParentBarcode'+suffix,
    ))
    ca.merge(sub)
